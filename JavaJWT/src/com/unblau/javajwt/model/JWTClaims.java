package com.unblau.javajwt.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class JWTClaims implements Serializable
{
	private static final long serialVersionUID = 1L;

	/**
	 * @see <a href="http://self-issued.info/docs/draft-ietf-oauth-json-web-token.html#issDef">Issuer Claim</a>
	 */
	private String iss;
	
	/**
	 * @see <a href="http://self-issued.info/docs/draft-ietf-oauth-json-web-token.html#subDef">Subject Claim</a>
	 */
	private String sub;
	
	/**
	 * @see <a href="http://self-issued.info/docs/draft-ietf-oauth-json-web-token.html#audDef">Audience Claim</a>
	 */
	private String aud;
	
	/**
	 * @see <a href="http://self-issued.info/docs/draft-ietf-oauth-json-web-token.html#expDef">Expiration Time Claim</a>
	 */
	private long exp;
	
	/**
	 * @see <a href="http://self-issued.info/docs/draft-ietf-oauth-json-web-token.html#iatDef">Issued At Claim</a>
	 */
	private long iat;
	
	public JWTClaims() {}
	
	public String getIss() {return iss;}
	public void setIss(String iss) {this.iss = iss;}
	
	public String getSub() {return sub;}
	public void setSub(String sub) {this.sub = sub;}
	
	public String getAud() {return aud;}
	public void setAud(String aud) {this.aud = aud;}

	public long getExp() {return exp;}
	public void setExp(long exp) {this.exp = exp;}
	
	public long getIat() {return iat;}
	public void setIat(long iat) {this.iat = iat;}
}