'use strict';

/**
 * binding web services in Google Cloud EndPoints
 */
angular.module('app').service('endpointSrv', ['$http', 'alertSrv', function($http, alertSrv) {

	/**
	 * GCE URL
	 */
	var API = window.location.hostname=='localhost' ? 'http://localhost:8888/_ah/api/javaJWT/v1' : 'https://subtle-poet-488.appspot.com/_ah/api/javaJWT/v1';

	/**
	 * Verify JWT
	 */
	this.verifyJWT = function (jwt, successFnc, errorFnc) {
		$http.post(API + '/verify', jwt)
		.success(function() {
    		alertSrv.addAlert("success", "Great !!! you have passed all the checks");
			if (successFnc != null) successFnc();
		}).error(function(err, status, headers, config) {
			printHTTPError(err, status, headers, config)
			if (errorFnc != null) errorFnc();
		});
	};

    /**
     * print HTTP error
     */
	function printHTTPError(err, status, headers, config)
	{	
    	// print to console
    	console.error("HTTP error. Code: " + status);
    	console.error(err);
    	
    	// print to user
    	if (typeof err == 'string') {
    		alertSrv.addAlert("danger", err);
    	} else {
    		alertSrv.addAlert("danger", err.error.message || "HTTP error. Code: " + status);
    	}
    }
	
}]);