'use strict';

/**
 * binding for an alerts service
 */
angular.module('app').service('alertSrv', [function() {

	/**
	 * An array containing the alerts
	 * We must expose directly the object. We cannot create a getAlerts() instead of, it didn't run
	 */
	this.alerts = [];
    
    /**
     * Adds an alert to container
     * Allowed alert types: "success"(green), "info"(blue), "warning"(orange) or "danger"(pink)
     */
	this.addAlert = function(type, msg) {
    	this.alerts.push({'type': type, 'msg': msg});
    };
    
    /**
     * Removes an alert of the container
     */
    this.removeAlert = function(index) {
    	this.alerts.splice(index, 1);
    };
    
    /**
     * Counts the current alerts
     */
    this.getCount = function() {
    	return this.alerts.length;
    };
    
}]);