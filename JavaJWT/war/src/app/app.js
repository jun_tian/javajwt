'use strict';

angular.module('app', ['ngRoute', 'ngCookies', 'ui.bootstrap', 'rootCtrl'])

/**
 * Global constants 
 */
.constant('APPNAME', 'Java JWT')
                       
/**
 * Configuration: routing 
 */
.config(function($routeProvider) {
    
	// route configuration
	$routeProvider.when('/', {templateUrl:'src/app/root.html', controller:'rootCtrl'});
	$routeProvider.otherwise({redirectTo:'/'});
	
});

